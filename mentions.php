<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- encodage de la page -->
    <meta charset="UTF-8">
    <!-- format de la page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- titre de la page  -->
    <title>Mentions • Therence Ferron</title>
    <!-- description de la page -->
    <meta name="description" content="Mentions légales du portfolio de Therence Ferron">

    <!-- Opengraph -->
    <?php include('includes/og.php'); ?>
    
    <!-- Twitter -->
    <?php include('includes/twitter.php'); ?>

    <!-- CSS -->
    <?php include('includes/css.php'); ?>

</head>
<body>

    <!-- HEADER + NAV + SURCOUCHE -->
    <?php include('includes/header-surcouche.php'); ?>

    <!-- section 3 - content -->
    <section class="block-section section-title mentions">
        <div class="section-inner">
            <div class="container">
                <br>
                <br>
                <br>
                <br>
                <br>
                <p class="paragraph">
                    Réalisation du site internet :<br>
                    Therence Ferron<br>
                    Veuillez trouver les moyens de me contacter sur <a href="contact" class="link"> cette page</a>.
                </p>
                <p class="paragraph">

                Thérence Ferron est propriétaire du contenu de ce site Web qu’il a mis en ligne.
                <br>
                Le contenu de ce site est protégé par la propriété littéraire et artistique, la convention de Berne et le code de la propriété intellectuelle, Livre I.
                <br>
                Le contenu de ce site (notamment données, informations, illustrations, etc. …) est protégé par droit d’auteur et autres droits de propriété intellectuelle. Toute reproduction ou représentation totale ou partielle de ce site par quelque procédé que ce soit, sans autorisation expresse, est interdite et constituerait une contrefaçon sanctionnée par les articles L.335-2 et suivants du Code de la propriété intellectuelle.
                <br>
                En conséquence, l’utilisateur du site s’interdit de :
                <br>
                Toute reproduction totale ou partielle des logos, images, scripts, code HTML, balises meta effectuée à partir des éléments du site sans notre autorisation expresse est prohibée au sens de l’article L.713-2 du Code de la propriété intellectuelle.
                <br>
                Toute copie, reproduction, diffusion, intégrale ou partielle, du contenu du site par quelque procédé que ce soit est illicite à l’exception d’une unique copie sur un seul ordinateur et réservée à l’usage exclusivement privé du copiste.
                <br>
                Ce site s'est inspiré du design de <a target="_blank" href="https://www.marinatureczek.com/ ">Marina Tureczek</a> et utilise la palette de couleurs  <a target="_blank" href="https://www.nordtheme.com/">Nord</a>
                <br>
                Sur ce site j'utilise <a target="_blank" href="https://jquery.com/">Jquery</a>,
                <a target="_blank" href="https://getbootstrap.com/">Bootstrap</a>,
                <a target="_blank" href="https://owlcarousel2.github.io/OwlCarousel2/">Owl-Carousel</a>,
                <a target="_blank" href="http://fontello.com/">Fontello</a>,
                <a target="_blank" href="https://apoorv.pro/lozad.js/#example-with-video">Lozad</a>,
                <a target="_blank" href="https://schema.org/">Schema.org</a>,
                <a target="_blank" href="https://developers.facebook.com/tools/debug/?q=https%3A%2F%2Ftherenceferron.fr%2F">OpenGraph</a>
                ainsi que
                <a target="_blank" href="https://developer.twitter.com/en/docs/tweets/optimize-with-cards/guides/getting-started">Twitter Card</a>.
                


            </p>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php

        include_once('includes/footer.php');

    ?>

    <!-- Script -->
    <?php include('includes/script.php'); ?>
</body>
</html>