<!-- Reset -->
<link rel="stylesheet" href="style/reset.css">
<!-- Bootstrap -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<!-- Owl carousel -->
<link rel="stylesheet" href="owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="owl-carousel/owl.theme.default.min.css">
<!-- App CSS -->
<link rel="stylesheet" href="style/style.css">