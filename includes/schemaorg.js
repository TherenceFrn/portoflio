<script type="application/ld+json">
    { "@context": "http://schema.org/",
        "@type" : "Corporation",
        "name" : "Thérence Ferron",
        "url" : "https://therenceferron.fr",
        "description" : "Thérence Ferron est un étudiant en développement web à MyDigitalSchool Angers en deuxième année de Bachelor.",
        "image" : "http://therenceferron.fr/media/seo/preview.png",
        // "image" : "http://therenceferron.fr/media/icon/icone.jpg",
        "telephone" : "+33 647794708"
    }
</script>   