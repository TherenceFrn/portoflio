<footer class="block-footer block-section">
    <div class="section-inner">
        <div class="container">

            Copyright ©
            <?php $the_year = date("Y"); echo $the_year; ?>
            <a href="https://therenceferron.fr/"> therence.fr </a>
            - Tous droits réservés - 
            <a href="mentions"> Mentions légales </a>
            
        </div>
    </div>
</footer>