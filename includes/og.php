  <!-- Pas de traduction -->
  <meta name="google" content="notranslate">
  <!-- Canonical -->
  <link rel="canonical" href="http://therenceferron.fr/index">
  <!-- ICONE : -->
  <link rel="shortcut icon" href="media/icon/icone.ico" type="image/x-icon">
  <!-- OG title -->
  <meta property="og:title" content="Portfolio de Therence Ferron">
  <!-- OG type -->
  <meta property="og:type" content="website">
  <!-- OG url -->
  <meta property="og:url" content="http://therenceferron.fr/">
  <!-- OG image -->
  <meta property="og:image" content="http://therenceferron.fr/media/seo/preview.png">
  <!-- Og description de l'image -->
  <meta property="og:image:alt" content="Logo du Portfolio de Therence Ferron">
  <!-- OG description -->
  <meta property="og:description" content="Découvrez le portfolio de Thérence Ferron, un étudiant qui souhaite devenir développeur web. Ce portfolio expose tous les projets sur lesquels il a travaillé.">
  <!-- OG Langue du site -->
  <meta property="og:locale" content="fr_FR">
  <!-- OG Nom du site -->
  <meta property="og:site_name" content="Portfolio de Therence Ferron">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166282954-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-166282954-1');
  </script>
  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css2?family=Manrope:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet"> 
  <!-- LazyLoad -->
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>