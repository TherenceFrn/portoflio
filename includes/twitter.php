    <!-- twitter card -->
    <meta name="twitter:card" content="summary_large_image">
    <!-- twitter site -->
    <meta name="twitter:site" content="https://therenceferron.fr/">
    <!-- twitter title -->
    <meta name="twitter:title" content="Portfolio de Therence Ferron">
    <!-- twitter description -->
    <meta name="twitter:description" content="Découvrez le portfolio de Thérence Ferron, un étudiant qui souhaite devenir développeur web. Ce portfolio expose tous les projets sur lesquels il a travaillé">
    <!-- twitter creator -->
    <meta name="twitter:creator" content="@therenceferron">
    <!-- twitter image:src -->
    <meta name="twitter:image:src" content="http://therenceferron.fr/media/seo/preview.png">
    <!-- twitter description de l'image -->
    <meta name="twitter:image:alt" content="Logo du Portfolio de Therence Ferron">