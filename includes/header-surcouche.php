<!-- Nav + Header :  -->
    <section class="block-section section-surcouche">
        <header class="block-header">
            <div class="row">
                <div class="col-4 header-column"></div>
                <div class="col-8 col-sm-4 header-column">
                    <nav class="block-nav">
                        <ul class="menu">
                            <li class="item">
                                <a href="https://therenceferron.fr/">
                                    <h2 class="title">
                                        Index
                                    </h2>   
                                </a>
                            </li>
                            <li class="item">
                                <a href="cv">
                                    <h2 class="title">
                                        CV
                                    </h2>   
                                </a>
                            </li>
                            <li class="item">
                                <a href="projets">
                                    <h2 class="title">
                                        Projets
                                    </h2>   
                                </a>
                            </li>
                            <li class="item">
                                <a href="realisations">
                                    <h2 class="title">
                                        Réalisations
                                    </h2>   
                                </a>
                            </li>
                            <li class="item">
                                <a href="contact">
                                    <h2 class="title">
                                        Contact
                                    </h2>   
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-0 col-sm-4 header-column"></div>
            </div>
        </header>
    </section>

    <!-- Surcouche  -->
    <div class="index-link">
        <span class="title">
            <a href="https://therenceferron.fr/" class="link">
                Thérence Ferron
            </a>
        </span>
    </div>
    <div class="surcouche-bar left-bar"></div>
    <div class="surcouche-bar top-bar"></div>
    <div class="surcouche-bar right-bar"></div>
    <div class="surcouche-bar bot-bar"></div>
    <div class="menu-button off">
        <span class="menu-text">
            Menu
        </span >
        <div class="menu-button-bars">
            <div class="menu-button-bar menu-button-bar-1"></div>
            <div class="menu-button-bar menu-button-bar-2"></div>
            <div class="menu-button-bar menu-button-bar-3"></div>
        </div>
    </div>