    <!-- JQUery -->
    <script src="https://code.jquery.com/jquery-3.5.0.min.js"
        integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
    <!-- MouseWheel -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script> -->
    <!-- Owl Carousel -->
    <script src="owl-carousel/owl.carousel.min.js">
    </script>
    <!-- PERSO -->
    <script src="script/script.js">
    </script>
    <!-- LazyLoad -->
    <script>
        const observer = lozad();
        observer.observe();
    </script>
    <?php
        include_once('schemaorg.js');
    ?>