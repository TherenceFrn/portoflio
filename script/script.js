$('.menu-button.off').on('click', function(){
    console.log('il a cliqué'+this);

    if($(this).hasClass('on')){
        
        console.log('il a la class ON');
        
        $('.block-section.section-surcouche').animate({
            top: '0',
            left: '-100%'
        },

        "slow",
        
        // "ease",
        
        function() {
            console.log('Fin de l animation pour rentrer')
            $('.block-section.section-surcouche').hide();
        });


        $(this).addClass('off').removeClass('on');
        
    }else{

        console.log('il a la class OFF');
        
        $('.block-section.section-surcouche').show();

        $('.block-section.section-surcouche').animate({
            top: '0',
            left: '0'
        },

        "slow",
        
        // "ease",

        function() {
            console.log('Fin de l animation pour entrer');
        });



        $(this).addClass('on').removeClass('off');
    }

})


// Owl Carousel, Page Projet
var owlProjet = $('.owl-carousel-projet');
$('.owl-carousel-projet').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    navigation: false,
    dots: false,
    // autoWidth: true,
    nav: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 3
        }
    }
});


/* Images Page Réalisation */

$('figure.realisations-item').on('click', function(){

    // data id de l'element sur lequel on a cliqué 
    var dataId = $(this).attr('data-id');

    // syntaxe vers l'image de l'element sur lequel on a cliqué
    var oui = "figure.realisations-item[data-id="+dataId+"] .img-realisations";
    
    // var true / false si l'element a la class "selected"
    var ouiHasClass = $(oui).hasClass('selected');

    // en fonction de si l'element a deja la class on va lui retirer ou lui ajouter
    if (ouiHasClass == true) {
        // il a deja la class donc on lui retire a lui et a tous les autres
        $('figure.realisations-item img').removeClass('selected');
        $('.realisations-content[data-id=' + dataId + ']').animate({
            transform: 'translate(-100%, 0)'
        }, "slow");

        // on le display none
        $('.realisations-content').delay('400').hide();

    }else{
        // il ne l'a pas donc on le retire a tous les autres puis on lui rajoute
        $('figure.realisations-item img').removeClass('selected');
        $(oui).addClass('selected');

        // on les cache tous + on show celui qu'on souhaite
        $('.realisations-content').hide();
        $('.realisations-content[data-id=' + dataId + ']').show();

        // on anime pour le faire venir du haut
        // $('.realisations-content[data-id=' + dataId + ']').animate({

        //    transform: 'translate(0, 0)',
        //    border: '1px solid red'
        
        // }, function(){
        //     console.log('animation terminée');
        // });

        // $('.realisations-content[data-id=' + dataId + ']').css('transform', 'translate(0, 0)');
    }

});



/* FILTRE REALISATIONS */

$('.r-filter .r-item').on('click', function(){

    // catégorie sélectionnée
    var dataClass = $(this).attr('data-class');
    console.log('Catégorie sélectionnée : '+dataClass);

    // on test si la catégorie qu'on a select a deja ete select
    var ouiHasClass = $(this).hasClass('r-selected');
    $('.realisations-content').fadeOut();
    
    if(ouiHasClass ==  true){

        $('.r-filter .r-item').removeClass('r-selected');
        
        $('.realisations-item').fadeIn();
        
    }else{
        
        $('.r-filter .r-item').removeClass('r-selected');
        $(this).addClass('r-selected');

        $('.realisations-item').fadeOut();
        $('.realisations-item.'+dataClass).fadeIn();

    }

});


// LAZY LOAD 






// Disable Right Click

(function ($) {
    $(document).on('contextmenu', 'img', function () {
        return false;
    })
})(jQuery);

(function ($) {
    $(document).on('contextmenu', 'video', function () {
        return false;
    })
})(jQuery);


// Scroll spy

window.onscroll = function () { scrollFunction() };

function scrollFunction() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {

        // document.getElementById("scrollspy").style.display = "none";
        $('#scrollspy').hide('slow');
    } else {
        
        $('#scrollspy').show('slow');
        // document.getElementById("scrollspy").style.display = "block";
    }
} 


// Erreur form HIDE

$('.erreur-form').on('click', function(){

    $(this).hide('slow');
})