<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- encodage de la page -->
    <meta charset="UTF-8">
    <!-- format de la page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- titre de la page  -->
    <title>Réalisations • Therence Ferron</title>
    <!-- description de la page -->
    <meta name="description" content="Voici tous les contenus que j'ai créé depuis 2 ans, on peut y trouver des montages photos, des dessins, des photos, des vidéos, des animations.">
    <!-- keywords -->
    <meta name="keywords" content="developpeur web, angers, therence ferron, photoshop, illustrator, projets, blender, after effect">
    <!-- author -->
    <meta name="author" content="Therence Ferron">
    <!-- copyright -->
    <meta name="copyright" content="Therence Ferron">
    <!-- Opengraph -->
    <?php include('includes/og.php'); ?>
    <!-- Twitter -->
    <?php include('includes/twitter.php'); ?>
    <!-- CSS -->
    <?php include('includes/css.php'); ?>


</head>
<body>

    <!-- Section 1 -->
    <section class="block-section section-content">
        <div class="section-inner">
            <div class="projet-header">
                <!-- <figure class="projet-img">
                    <img src="media/image/projets-list/logo-japon-sans-texte.svg" alt="Therence Ferron Blog au Japon Developpeur Web Projet">
                </figure> -->
                <h1 class="title">
                    Réalisations
                </h1>
            </div>
        </div>
        <div id="scrollspy">
        </div>
    </section>

    <!-- HEADER + NAV + SURCOUCHE -->
    <?php include('includes/header-surcouche.php'); ?>
    
    <!-- section 3 - Slider -->
    <section class="block-section section-title section-projet-title section-projet-slider">
        <div class="section-inner">
            <div class="container">
                <div class="owl-carousel owl-carousel-projet owl-theme">
                    <div class="item">
                        <!-- <h4>1</h4> -->
                        <img src="media/image/realisations/3d/boule-de-poil.jpg" alt="Boule de poil réalisé sur Blender par Thérence Ferron">
                    </div>
                    <div class="item">
                        <!-- <h4>2</h4> -->
                        <img src="media/image/realisations/3d/puit.jpg"  alt="Puit style cartoon réalisé sur Blender par Thérence Ferron">
                    </div>
                    <div class="item">
                        <!-- <h4>3</h4> -->
                        <img src="media/image/realisations/3d/donut-render-9.jpg" alt="Donut photoréaliste réalisé sur Blender par Thérence Ferron">
                    </div>
                    <div class="item">
                        <!-- <h4>4</h4> -->
                        <img src="media/image/realisations/3d/epee-cartoon.jpg" alt="Epée style cartoon réalisé sur Blender par Thérence Ferron">
                    </div>
                     <div class="item">
                        <!-- <h4>5</h4> -->
                        <img src="media/image/realisations/3d/wallpaper-fluid.jpg" alt="Fond d'écran style fluide réalisé sur Blender par Thérence Ferron">
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="block-section section-title section-realisation-filter">
        <div class="section-inner">
            <div class="container">
                <ul class="r-filter row">
                    <li class="r-item col-6 col-sm-4 col-md-3 col-lg-2" data-class="img">Images</li>
                    <li class="r-item col-6 col-sm-4 col-md-3 col-lg-2" data-class="video">Vidéos</li>
                    <li class="r-item col-6 col-sm-4 col-md-3 col-lg-2" data-class="anim">Animations</li>
                    <li class="r-item col-6 col-sm-4 col-md-3 col-lg-2" data-class="photo">Photos</li>
                    <li class="r-item col-6 col-sm-4 col-md-3 col-lg-2" data-class="draw">Dessins</li>
                    <li class="r-item col-6 col-sm-4 col-md-3 col-lg-2" data-class="3d">3D</li>
                </ul>
            </div>
        </div>
    </section>

    <section class="block-section section-title section-realisations-liste">
        <div class="section-inner">
            <div class="container">
                <div class="row">

                    <!-- 
                        <div>
                            <figure class="col-6 col-sm-4 col-md-3 col-lg-2">
                                <img src="media/image/realisations/images/kirua.png" class="img-portfolio" alt="" type="button" data-toggle="modal" data-target=".bd-example-modal-lg-1-1">
                            </figure>
                            
                            <div class="modal fade bd-example-modal-lg-1-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <img src="media/image/realisations/images/kirua.png" alt="" >
                                    </div>
                                    <p class="modal-paragraph paragraph" style="background-color: white; margin: 1px; padding: 10px;">Description</p>
                                </div>
                            </div>
                        </div>
                    -->

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item 3d" data-id="0">
                        <img data-src="media/image/realisations/3d/boule-de-poil.jpg" class="img-realisations lozad" alt="Boule de poil réalisé sur Blender par Thérence Ferron">
                        <div class="realisations-content" data-id="0">
                            <p class="paragraph">
                                Logiciel : Blender
                                <br>
                                Date : Mai 2020
                                <br>

                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item 3d" data-id="1">
                        <img data-src="media/image/realisations/3d/donut-render-9.jpg" class="img-realisations lozad" alt="Donut photoréaliste réalisé sur Blender par Thérence Ferron">
                        <div class="realisations-content" data-id="1">
                            <p class="paragraph">
                                Logiciel : Blender
                                <br>
                                Date : Mai 2020
                                <br>
                            </p>
                        </div>
                    </figure>


                    <figure class="col-6 col-sm-4 col-md-3 realisations-item 3d" data-id="2">
                        <img data-src="media/image/realisations/3d/epee-cartoon.jpg" class="img-realisations lozad" alt="Epée style cartoon réalisé sur Blender par Thérence Ferron">
                        <div class="realisations-content" data-id="2">
                            <p class="paragraph">
                                Logiciel : Blender
                                <br>
                                Date : Mai 2020
                                <br>
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item 3d" data-id="3">
                        <img data-src="media/image/realisations/3d/puit.jpg" class="img-realisations lozad" alt="Puit style cartoon réalisé sur Blender par Thérence Ferron">
                        <div class="realisations-content" data-id="3">
                            <p class="paragraph">
                                Logiciel : Blender
                                <br>
                                Date : Mai 2020
                                <br>
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item 3d" data-id="4">
                        <img data-src="media/image/realisations/3d/wallpaper-fluid.jpg" class="img-realisations lozad" alt="Fond d'écran style fluide réalisé sur Blender par Thérence Ferron">
                        <div class="realisations-content" data-id="4">
                            <p class="paragraph">
                                Logiciel : Blender
                                <br>
                                Date : Mai 2020
                                <br>
                            </p>
                        </div>
                    </figure>

                    <!-- ANIM -->

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item anim" data-id="6">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/animations/fluide.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="6">
                            <p class="paragraph">
                                Logiciel : Adobe Animate
                                <br>
                                Date : Février 2019
                                <br>
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item anim" data-id="7">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/animations/grenouille.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="7">
                            <p class="paragraph">
                                Logiciel : After Effect
                                <br>
                                Date : Décembre 2019
                                <br>
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item anim" data-id="8">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/animations/charlie.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="8">
                            <p class="paragraph">
                                Logiciel : After Effect
                                <br>
                                Date : Octobre 2019
                                <br>
                            </p>
                        </div>
                    </figure>
                    
                    <figure class="col-6 col-sm-4 col-md-3 realisations-item anim" data-id="9">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/animations/carre-1.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="9">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item anim" data-id="10">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/animations/carre-2.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="10">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item anim" data-id="11">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/animations/logo.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="11">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                     <figure class="col-6 col-sm-4 col-md-3 realisations-item anim" data-id="12">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/animations/MD.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="12">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <!-- DRAW -->

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="13">
                        <img data-src="media/image/realisations/dessins/pikachu.png" class="img-realisations lozad" alt="Pikachu dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="13">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="14">
                        <img data-src="media/image/realisations/dessins/kirua.png" class="img-realisations lozad" alt="Kirua dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="14">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>
                  
                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="15">
                        <img data-src="media/image/realisations/dessins/gon.png" class="img-realisations lozad" alt="Gon dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="15">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="16">
                        <img data-src="media/image/realisations/dessins/kyll.png" class="img-realisations lozad" alt="Personnage dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="16">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="17">
                        <img data-src="media/image/realisations/dessins/ness.png" class="img-realisations lozad" alt="Ness dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="17">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="18">
                        <img data-src="media/image/realisations/dessins/mob.png" class="img-realisations lozad" alt="Mob dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="18">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="19">
                        <img data-src="media/image/realisations/dessins/pepe-1.png" class="img-realisations lozad" alt="Pepe dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="19">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    
                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="20">
                        <img data-src="media/image/realisations/dessins/pepe-2.png" class="img-realisations lozad" alt="Pepe dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="20">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="21">
                        <img data-src="media/image/realisations/dessins/pepe-3.png" class="img-realisations lozad" alt="Pepe dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="21">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>
                    
                    <figure class="col-6 col-sm-4 col-md-3 realisations-item draw" data-id="22">
                        <img data-src="media/image/realisations/dessins/pepe-4.png" class="img-realisations lozad" alt="Pepe dessiné sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="22">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <!-- IMG -->

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="23">
                        <img data-src="media/image/realisations/images/affiche.png" class="img-realisations lozad" alt="Affiche de 2001 odyssée de l'espace réalisé sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="23">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="24">
                        <img data-src="media/image/realisations/images/akira.png" class="img-realisations lozad" alt="Pillule d'akira réalisé sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="24">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                     <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="25">
                        <img data-src="media/image/realisations/images/anime.png" class="img-realisations lozad" alt="Render d'un personnage réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="25">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="26">
                        <img data-src="media/image/realisations/images/banniere.png" class="img-realisations lozad" alt="Banniere réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="26">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="27">
                        <img data-src="media/image/realisations/images/bluewavy.png" class="img-realisations lozad" alt="Image réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="27">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>


                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="28">
                        <img data-src="media/image/realisations/images/bucket.png" class="img-realisations lozad" alt="Image réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="28">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="29">
                        <img data-src="media/image/realisations/images/fils.png" class="img-realisations lozad" alt="Image réalisé sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="29">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="30">
                        <img data-src="media/image/realisations/images/flatdesigncarac.png" class="img-realisations lozad" alt="Personnages en flat design réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="30">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>
                    
                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="31">
                        <img data-src="media/image/realisations/images/galaxy.png" class="img-realisations lozad" alt="Galaxy réalisé sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="31">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                     
                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="32">
                        <img data-src="media/image/realisations/images/gon.png" class="img-realisations lozad" alt="Gon réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="32">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="33">
                        <img data-src="media/image/realisations/images/landscape.png" class="img-realisations lozad" alt="Image réalisé sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="33">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="3d">
                        <img data-src="media/image/realisations/images/lanetechelou.png" class="img-realisations lozad" alt="Planetes réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="3d">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="35">
                        <img data-src="media/image/realisations/images/liquie3d.png" class="img-realisations lozad" alt="Image réalisé sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="35">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="36">
                        <img data-src="media/image/realisations/images/mermaid-teet.png" class="img-realisations lozad" alt="Personnage réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="36">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="37">
                        <img data-src="media/image/realisations/images/moi.png" class="img-realisations lozad" alt="Personnage réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="37">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <!-- <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="38">
                        <img data-src="media/image/realisations/images/piece-fazer.png" class="img-realisations lozad" alt="Pièce réalisé sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="38">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure> -->

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="39">
                        <img data-src="media/image/realisations/images/planetes.png" class="img-realisations lozad" alt="Planètes réalisés sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="39">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="40">
                        <img data-src="media/image/realisations/images/rei.png" class="img-realisations lozad" alt="Rei réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="40">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="41">
                        <img data-src="media/image/realisations/images/therence-ferron.png" class="img-realisations lozad" alt="Bannière réalisé sur Photoshop par Thérence Ferron">
                        <div class="realisations-content" data-id="41">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="42">
                        <img data-src="media/image/realisations/images/tv.png" class="img-realisations lozad" alt="Image réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="42">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="43">
                        <img data-src="media/image/realisations/images/van-canyon.png" class="img-realisations lozad" alt="Image réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="43">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item img" data-id="44">
                        <img data-src="media/image/realisations/images/ZEF.png" class="img-realisations lozad" alt="Logo de Zen En Famille réalisé sur Illustrator par Thérence Ferron">
                        <div class="realisations-content" data-id="44">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <!-- PHOTOS -->

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="45">
                        <img data-src="media/image/realisations/photos/1.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron au TeamLab Bordeless à Tokyo au Japon">
                        <div class="realisations-content" data-id="45">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="46">
                        <img data-src="media/image/realisations/photos/2.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron au TeamLab Bordeless à Tokyo au Japon">
                        <div class="realisations-content" data-id="46">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="47">
                        <img data-src="media/image/realisations/photos/3.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron au TeamLab Bordeless à Tokyo au Japon">
                        <div class="realisations-content" data-id="47">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="48">
                        <img data-src="media/image/realisations/photos/4.JPG" class="img-realisations lozad" alt="Photo prise par Thérence Ferron au TeamLab Bordeless à Tokyo au Japon">
                        <div class="realisations-content" data-id="48">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="49">
                        <img data-src="media/image/realisations/photos/5.jpeg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron sur l'île d'Odaiba à Tokyo au Japon">
                        <div class="realisations-content" data-id="49">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="50">
                        <img data-src="media/image/realisations/photos/5.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Tokyo au Japon">
                        <div class="realisations-content" data-id="50">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="51">
                        <img data-src="media/image/realisations/photos/6.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Osaka au Japon">
                        <div class="realisations-content" data-id="51">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="52">
                        <img data-src="media/image/realisations/photos/7.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Kyoto au Japon">
                        <div class="realisations-content" data-id="52">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>


                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="54">
                        <img data-src="media/image/realisations/photos/8.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Oyado Hana au Japon">
                        <div class="realisations-content" data-id="54">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="55">
                        <img data-src="media/image/realisations/photos/9.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Kumano Nachi no taki au Japon">
                        <div class="realisations-content" data-id="55">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="56">
                        <img data-src="media/image/realisations/photos/10.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Kumano Nachi no taki au Japon">
                        <div class="realisations-content" data-id="56">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="57">
                        <img data-src="media/image/realisations/photos/11.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Kyoto au Japon">
                        <div class="realisations-content" data-id="57">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="58">
                        <img data-src="media/image/realisations/photos/12.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Tokyo au Japon">
                        <div class="realisations-content" data-id="58">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="59">
                        <img data-src="media/image/realisations/photos/13.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Tokyo au Japon">
                        <div class="realisations-content" data-id="59">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="60">
                        <img data-src="media/image/realisations/photos/14.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron au TeamLab Bordeless à Tokyo au Japon">
                        <div class="realisations-content" data-id="60">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item photo" data-id="61">
                        <img data-src="media/image/realisations/photos/15.jpg" class="img-realisations lozad" alt="Photo prise par Thérence Ferron à Osaka au Japon">
                        <div class="realisations-content" data-id="61">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <!-- VIDEOS -->

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item video" data-id="62">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/videos/RENDU_FINAL.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="62">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item video" data-id="63">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/videos/mcdo.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="63">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item video" data-id="64">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/videos/comp.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="64">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    <figure class="col-6 col-sm-4 col-md-3 realisations-item video" data-id="65">
                        <video class="img-realisations lozad" autoplay muted controls alt="">
                            <source data-src="media/image/realisations/videos/zenzile.mp4" type="video/mp4">
                        </video>
                        <div class="realisations-content" data-id="65">
                            <p class="paragraph">
                                Thérence Bloc 0
                            </p>
                        </div>
                    </figure>

                    


                </div> 
            </div>
        </div>
    </section>

      <!-- Footer -->
    <?php

        include_once('includes/footer.php');

    ?>

      

    <!-- Script -->
    <?php include('includes/script.php'); ?>

</body>
</html>