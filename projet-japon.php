<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- encodage de la page -->
    <meta charset="UTF-8">
    <!-- format de la page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- titre de la page  -->
    <title>Projet Japon • Therence Ferron</title>
    <!-- description de la page -->
    <meta name="description" content="Durant l'été 2019 je suis parti 3 semaines au Japon, j'ai donc décidé de créer un blog pour publier quotidiennement des articles au sujet de mon voyage.">
    <!-- keywords -->
    <meta name="keywords" content="developpeur web, angers, therence ferron, voyage, japon, projet, blog, php">
    <!-- author -->
    <meta name="author" content="Therence Ferron">
    <!-- copyright -->
    <meta name="copyright" content="Therence Ferron">


    <!-- Opengraph -->
    <?php include('includes/og.php'); ?>
    
    <!-- Twitter -->
    <?php include('includes/twitter.php'); ?>

    <!-- CSS -->
    <?php include('includes/css.php'); ?>

</head>
<body>
    <!-- Retourner à la liste des projets : -->
    <div class="back-to-projets">
        <a href="projets">
            Retourner à la liste des projets
        </a>
    </div>

    <!-- Section 1 -->
    <section class="block-section section-content">
        <div class="section-inner">
            <div class="projet-header">
                <figure class="projet-img">
                    <img src="media/image/projets-list/logo-japon-sans-texte.svg" alt="Therence Ferron Blog Japon Developpeur Web Projet">
                </figure>
                <h1 class="title">
                    Blog au Japon
                </h1>
            </div>
        </div>
        <div id="scrollspy">
        </div>
    </section>

    <!-- HEADER + NAV + SURCOUCHE -->
    <?php include('includes/header-surcouche.php'); ?>

    <!-- Section 2 - title -->
    <section class="block-section section-title section-projet-title">
        <div class="section-inner">
            <div class="projet-title">
                <h3 class="title">
                    Le projet
                </h3>
                <img class="projet-pepe" title="Pepe (La masquotte de mon portfolio)" src="media/image/pepe/pepe-thinking-nobg-color.png" alt="Therence Ferron Pepe Developpement Web Angers">
            </div>
        </div>
    </section>

    <!-- section 3 - title -->
    <section class="block-section section-title">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                            Durant l'été 2019, je suis parti 3 semaines en voyage au Japon. Avant le début du voyage, j'ai décidé de créer un blog pour pouvoir poster tous les jours des images et conter mes aventures aux gens qui suivaient mon voyage, en particulier des gens de ma famille.
                        </p>
                    </div>
                    <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-japon/home.jpg" alt="Page d'accueil du projet blog japon de Thérence Ferron">
                    </div>
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                        Il fallait que je puisse poster des articles tous les jours, que je puisse poster des images, et mettre en forme le texte de l'article. Avoir un accès à un Back-office simple, dans lequel quelqu'un qui n'a jamais utilisé de BA puisse naviguer facilement.
                        <br>
                        Je voulais aussi intégrer une carte Google Map que j'avais préparée pour que chaque utilisateur puisse voir en détail quels lieux j'allais visiter. Enfin je souhaitais aussi avoir une page galerie, sur laquelle j'allais exposer les meilleurs clichés du voyage.
                        </p>
                    </div>
                      <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-japon/galerie.jpg" alt="Page galerie du projet blog japon de Thérence Ferron">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Section 2 - title -->
    <section class="block-section section-title section-projet-title">
        <div class="section-inner">
            <div class="projet-title">
                <h3 class="title">
                    Développement
                </h3>
                <img class="projet-pepe" title="Pepe (La masquotte de mon portfolio)" src="media/image/pepe/pepe-pc-bg-color.png" alt="Pepe réalisé par Thérence Ferron sur Photoshop">
            </div>
        </div>
    </section>

    <section class="block-section section-title">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                            Durant environ un mois, j'ai développé le site en PHP, en utilisant BootStrap (que je venais de découvrir en stage). J'ai décidé d'héberger le site avec un hébergeur gratuit, AlwaysData. Aujourd'hui le site n'est plus en ligne, mais il était disponible à <a href="https://celbilix.alwaysdata.net/" target="_blank" title="Ancien site blog Japon"> cette adresse</a>.
                        </p>
                    </div>
                    <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-japon/map.png" alt="Page map du projet blog japon de Thérence Ferron">
                    </div>
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                            J'ai tenté de faire le back-office le plus simple possible, afin que n'importe qui puisse poster des articles facilement. J'ai aussi décidé de créer un système de compte (+ inscription/connexion), afin de pouvoir permettre aux gens qui visitent le site de poster des commentaires sous chaque article.
                       </p>
                    </div>
                    <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-japon/article.png" alt="Page article du projet blog japon de Thérence Ferron">
                    </div>
                </div>
            </div>
        </div>
    </section>

        <!-- Section 2 - title -->
    <section class="block-section section-title section-projet-title">
        <div class="section-inner">
            <div class="projet-title">
                <h3 class="title">
                    Résultat
                </h3>
                <img class="projet-pepe" title="Pepe (La masquotte de mon portfolio)" src="media/image/pepe/pepe-simple-nobg-color.png" alt="Pepe réalisé par Thérence Ferron sur Photoshop">
            </div>
        </div>
    </section>

    
   

    <!-- section 3 - title -->
    <section class="block-section section-title">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                           Durant 3 semaines nous avons posté chaque jour des articles, alimenté la galerie et reçu un bon nombre de commentaires sous nos articles. Seulement une personne a créé un compte (😭).
                        </p>
                    </div>
                      <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-japon/profil.png" alt="Page profil du projet blog japon de Thérence Ferron">
                    </div>
                </div>
            </div>
        </div>
    </section>

     <!-- section 3 - title -->
    <section class="block-section section-title section-projet-slider">
        <div class="section-inner">
            <div class="container">
                <div class="owl-carousel owl-carousel-projet owl-theme">
                    <div class="item">
                        <!-- <h4>1</h4> -->
                        <img src="media/image/projet-japon/1.JPG" alt="Photo prise par Thérenece Ferron au Japon">
                    </div>
                    <div class="item">
                        <!-- <h4>2</h4> -->
                        <img src="media/image/projet-japon/5.jpg" alt="Photo prise par Thérenece Ferron au Japon">
                    </div>
                    <div class="item">
                        <!-- <h4>3</h4> -->
                        <img src="media/image/projet-japon/3.jpg" alt="Photo prise par Thérenece Ferron au Japon">
                    </div>
                    <div class="item">
                        <!-- <h4>4</h4> -->
                        <img src="media/image/projet-japon/4.jpg" alt="Photo prise par Thérenece Ferron au Japon">
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    <!-- section 3 - title -->
    <section class="block-section section-title">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                            Je vous mets en bonus quelques images du voyage.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

     <!-- Footer -->
    <?php

        include_once('includes/footer.php');

    ?>


<!-- Script -->
    <?php include('includes/script.php'); ?>

    <!-- Schema Org - Article -->

    
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "NewsArticle",
            "dateModified" : "2020-05-12",
            "datePublished" : "2020-05-12",
            "headline": "Projet de blog au Japon",
            "image": "https://therenceferron.fr/media/image/projet-japon/logo-japon-sans-texte.jpg",
            "author": {
                "@type" : "Corporation",
                "name" : "Thérence Ferron",
                "url" : "https://therenceferron.fr",
                "logo" : "https://therenceferron.fr/media/icon/icone.jpg"
            },
            "publisher": {
                "@type" : "Organization",
                "name" : "Thérence Ferron",
                "url" : "https://therenceferron.fr", 
                "logo": {
                    "@type": "ImageObject",
                    "name": "Logo Therence Ferron",
                    "width": "1000",
                    "height": "1000",
                    "url": "https://therenceferron.fr/media/icon/icone.jpg"
                }
            },
            "mainEntityOfPage" : "https://therenceferron.fr/projet-japon"
            }
    </script>

</body>
</html>