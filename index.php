<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- encodage de la page -->
    <meta charset="UTF-8">
    <!-- format de la page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- titre de la page  -->
    <title>Portfolio • Therence Ferron</title>
    <!-- description de la page -->
    <meta name="description" content="Découvrez le portfolio de Thérence Ferron, un étudiant qui souhaite devenir développeur web. Ce portfolio expose tous les projets sur lesquels il a travaillé.">
    <!-- keywords  -->
    <meta name="keywords" content="developpeur web, angers, therence ferron">
    <!-- Opengraph -->
    <?php include('includes/og.php'); ?>
    <!-- Twitter -->
    <?php include('includes/twitter.php'); ?>
    <!-- CSS -->
    <?php include('includes/css.php'); ?>

</head>
<body>

    <!-- Section 1 -->
    <section class="block-section section-content">
        <div class="section-inner">
            <div class="section1-index">
                <div class="section1-index-image">
                    <div class="section1-index-losange">
                        <img src="media/image/index/image-therence-2.jpeg" alt="Photo de Therence Ferron, développeur web à Angres, sur l'île d'Odaiba à Tokyo au Japon">
                    </div>
                    <div class="section1-rond">
                    </div>
                </div>
                <div class="section1-index-titre">
                    <h1 class="section1-index-titre-content">
                        Thérence
                    </h1>
                </div>
            </div>
        </div>
        <div id="scrollspy">
        </div>
    </section>

    <!-- HEADER + NAV + SURCOUCHE -->
    <?php include('includes/header-surcouche.php'); ?>

     <!-- Section 2 - title -->
    <section class="block-section section-title">
        <div class="section-inner">
            <h2 class="title">
                Profil
            </h2>
        </div>
    </section>

    <!-- section 3 - content -->
    <section class="block-section section-title">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <figure class="image">
                            <img class="lozad" data-src="media/image/index/profil-2.jpg" alt="Photo de Therence Ferron, développeur web à Angres, au TeamLab Borderless à Tokyo au Japon">
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 align-left">
                        <h3 class="title">
                            Je suis Thérence Ferron, un étudiant en bachelor web & design à MyDigitalSchool
                        </h3>
                        <p class="paragraph">
                            Après un Bac S option ISN, qui m'a permis d'avoir un premier contact avec le développement web, je suis entré à <a target="ablank" class="link" href="https://www.mydigitalschool.com/" rel="noopener">MyDigitalSchool</a> pour le bachelor Web & Digital.
                            <br>
                            Ce diplôme dure 3 ans, durant lequel nous avons deux ans de tronc commun puis un an de spécialisation. Durant ces deux années de tronc commun nous touchons aux trois axes de la formation: développement web, web design et web marketing.
                            <br>
                            Et c'est cet aspect de la formation qui m'a poussé à intégrer MDS, car le développement web m'attirait autant que le web design, auquel j'avais déjà un peu touché via Photoshop. De plus j'allais découvrir le web marketing, un domaine dont je n'avais aucune expérience.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- section 4 - content -->
    <section class="block-section section-title">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-6 align-left">
                        <h3 class="title">
                            Mon CV
                        </h3>
                        <p class="paragraph">
                            Vous pouvez télecharger mon CV grâce au lien à la fin de ce paragraphe. <br>
                            Si vous souhaitez me contacter, vous pouvez vous rendre sur la page <a target="" class="link" href="contact"> contact</a> où vous pourrez retrouver un formulaire de contact ainsi que les réseaux sociaux sur lesquels je suis présent.
                        </p>
                        <p class="paragraph">
                            <a href="media/document/CV_Therence_Ferron.pdf" class="link" target="_blank">Télecharger mon CV</a>
                        </p>
                        <p class="paragraph">
                            Vous pouvez aussi retrouver sur <a href="cv" title="Page CV" class="link"> cette page </a> un CV détaillé de mes compétences et de mes expériences.
                        </p>
                    </div>
                    <div class="col-12 col-sm-6">
                        <figure class="image">
                            <img class="lozad" data-src="media/document/CV_Therence_Ferron.jpg" alt="CV de Therence Ferron, développeur web à Angers">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <?php

        include_once('includes/footer.php');

    ?>

    <!-- Script -->
    <?php include('includes/script.php'); ?>
</body>
</html>