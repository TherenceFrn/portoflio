<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- encodage de la page -->
    <meta charset="UTF-8">
    <!-- format de la page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- titre de la page  -->
    <title>Erreur • Therence Ferron</title>
    <!-- description de la page -->
    <meta name="description" content="Il semblerait que vous vous soyez perdu">
    <!-- keywords -->
    <meta name="keywords" content="developpeur web, angers, therence ferron, projets">
    <!-- author -->
    <meta name="author" content="Therence Ferron">
    <!-- copyright -->
    <meta name="copyright" content="Therence Ferron">


    <!-- Opengraph -->
    <?php include('includes/og.php'); ?>
    
    <!-- Twitter -->
    <?php include('includes/twitter.php'); ?>

    <!-- CSS -->
    <?php include('includes/css.php'); ?>

</head>
<body>

    <!-- Section 1 -->
    <section class="block-section section-content error">
        <div class="section-inner">
            <div class="projet-header">
                <h1 class="title">
                    Il semblerait que vous vous soyez perdu
                </h1>
                <img src="media/image/error/lost.gif" alt="">
                <a href="https://therenceferron.fr/" class="link"> Retourner sur la page d'accueil</a>
            </div>
        </div>
    </section>
   
    <!-- HEADER + NAV + SURCOUCHE -->
    <?php include('includes/header-surcouche.php'); ?>

  
     <!-- Footer -->
    <?php

        include_once('includes/footer.php');

    ?>


    <!-- Script -->
    <?php include('includes/script.php'); ?>
</body>
</html>