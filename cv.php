<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- encodage de la page -->
    <meta charset="UTF-8">
    <!-- format de la page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- titre de la page  -->
    <title>CV • Therence Ferron</title>
    <!-- description de la page -->
    <meta name="description" content="Liste de mes compétences ainsi que de mes expériences, cette page est là pour enrichier les informations disponibles sur mon CV.">
    <!-- keywords -->
    <meta name="keywords" content="Developpeur Web, Angers, Therence Ferron, CV, Curriculum Vitae">
    <!-- author -->
    <meta name="author" content="Therence Ferron">
    <!-- copyright -->
    <meta name="copyright" content="Therence Ferron">


    <!-- Opengraph -->
    <?php include('includes/og.php'); ?>
    
    <!-- Twitter -->
    <?php include('includes/twitter.php'); ?>

    <!-- CSS -->
    <?php include('includes/css.php'); ?>

</head>
<body>

    <!-- Section 1 -->
    <section class="block-section section-content">
        <div class="section-inner">
            <div class="projet-header">
                <h1 class="title">
                    CV
                </h1>
            </div>
        </div>
        <div id="scrollspy">
        </div>
    </section>

    <!-- HEADER + NAV + SURCOUCHE -->
    <?php include('includes/header-surcouche.php'); ?>
    

     <!-- Section 2 - title -->
    <section class="block-section section-title little-padding">
        <div class="section-inner">
            <h2 class="title">
                Compétences
            </h2>
        </div>
    </section>

    <!-- section 3 - content -->
    <section class="block-section section-title section-competences">
        <div class="section-inner">
            <div class="container">
                <div class="skills">
                    <h3 class="title">
                        Développement Web
                    </h3>
                    <img class="skill-pepe" title="Pepe (La masquotte de mon portfolio)" src="media/image/pepe/pepe-pc-bg-color.png" alt="Therence Ferron développeur web à Angers">
                    <div class="skill-img skill-img-1">
                        <img src="media/image/cv/logo-html.png" title="HTML" alt="Icone de HTML, Therence Ferron développeur web à Angers">
                    </div>
                    <div class="skill-img skill-img-2">
                        <img src="media/image/cv/logo-css.png" title="CSS" alt="Icone de CSS, Therence Ferron développeur web à Angers">
                    </div>
                    <div class="skill-img skill-img-3">
                        <img src="media/image/cv/logo-js.png" title="JS" alt="Icone de JavaScript, Therence Ferron développeur web à Angers">
                    </div>
                        <div class="skill-img skill-img-4">
                        <img src="media/image/cv/logo-php.png" title="PHP" alt="Icone de PHP, Therence Ferron développeur web à Angers">
                    </div>
                    <div class="skill-img skill-img-5">
                        <img src="media/image/cv/logo-mysql.png" title="MySql" alt="Icone de MySql, Therence Ferron développeur web à Angers">
                    </div>
                    <div class="skill-img skill-img-6">
                        <img src="media/image/cv/logo-symfony.png" title="Symfony" alt="Icone de Symfony, Therence Ferron développeur web à Angers">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="block-section section-title section-competences">
        <div class="section-inner">
            <div class="container">
                <div class="skills">
                    <h3 class="title">
                        Autres compétences
                    </h3>
                    <img class="skill-pepe" title="Pepe (La masquotte de mon portfolio)" src="media/image/pepe/pepe-simple-bg-color.png" alt="Therence Ferron Pepe Developpement Web Angers">
                    <div class="skill-img skill-img-1">
                        <img src="media/image/cv/icone-scss.png" title="SCSS" alt="Therence Ferron Developpement Web Angers CSS SCSS">
                    </div>
                    <div class="skill-img skill-img-2">
                        <img src="media/image/cv/icone-bootstrap.png" title="Bootstrap" alt="Therence Ferron Developpement Web Angers CSS Bootstrap">
                    </div>
                    <div class="skill-img skill-img-3">
                        <img src="media/image/cv/logo-angular.png" title="JS" alt="Therence Ferron Developpement Web Angers Js Javascript Angular">
                    </div>
                        <div class="skill-img skill-img-4">
                        <img src="media/image/cv/icone-jquery.png" title="Jquery" alt="Therence Ferron Developpement Web Angers JS Javascript Angular">
                    </div>
                    <div class="skill-img skill-img-5">
                        <img src="media/image/cv/logo-blender.png" title="MySql" alt="Therence Ferron 3D Modelisation 3D Blender">
                    </div>
                    <div class="skill-img skill-img-6">
                        <img src="media/image/cv/logo-filezilla.png" title="FileZilla" alt="Therence Ferron Developpement Web Angers FileZilla">
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="block-section section-title section-competences">
        <div class="section-inner">
            <div class="container">
                <div class="skills">
                    <h3 class="title">
                        Suite Adobe
                    </h3>
                    <img class="skill-pepe" title="Pepe (La masquotte de mon portfolio)" src="media/image/pepe/pepe-pen-nobg-color.png" alt="Therence Ferron Pepe Developpement Web Angers Adobe Web Design">
                    <div class="skill-img skill-img-1">
                        <img src="media/image/cv/logo-ps.png" title="Photoshop" alt="Therence Ferron Developpement Web Angers CreativeCloud Photoshop">
                    </div>
                    <div class="skill-img skill-img-2">
                        <img src="media/image/cv/logo-ai.png" title="Illustrator" alt="Therence Ferron Developpement Web Angers CreativeCloud Illustrator">
                    </div>
                    <div class="skill-img skill-img-3">
                        <img src="media/image/cv/logo-pr.png" title="Premiere Pro" alt="Therence Ferron Developpement Web Angers CreativeCloud Premiere Pro">
                    </div>
                        <div class="skill-img skill-img-4">
                        <img src="media/image/cv/logo-ae.png" title="After Effect" alt="Therence Ferron Developpement Web Angers CreativeCloud After Effect">
                    </div>
                    <div class="skill-img skill-img-5">
                        <img src="media/image/cv/logo-xd.png" title="XD" alt="Therence Ferron Developpement Web Angers CreativeCloud Xd">
                    </div>
                    <div class="skill-img skill-img-6">
                        <img src="media/image/cv/logo-id.png" title="InDesign" alt="Therence Ferron Developpement Web Angers CreativeCloud InDesign">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="block-section section-title little-padding">
        <div class="section-inner">
            <p class="paragraph align-center">
                Les pages <a href="projets" class="link"> projets </a> et <a href="realisations" class="link"> réalisations </a> exposent ce que j'ai pu faire grâce à ces compétences. 
                <br>
                N'hésitez pas à aller y faire un tour.
            </p>
        </div>
    </section>

    

     <!-- Section 4 - title -->
    <section class="block-section section-title little-padding">
        <div class="section-inner">
            <h2 class="title">
                Expériences
            </h2>
        </div>
    </section>

    <!-- section 5 - experiences  -->
     <section class="block-section section-title section-experiences">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3 class="title">
                            Au comptoir de Noé
                        </h3>
                    </div>
                    <div class="col-12 col-sm-4 offset-sm-4">
                        <figure class="xp-img">
                            <img src="media/image/cv/logo-ACDN.png" alt="">
                        </figure>
                    </div>
                    <div class="col-12 col-sm-8 offset-sm-2">
                        <p class="paragraph">
                            Au Comptoir de Noé est un site e-commerce à cette <a class="link" href="https://aucomptoirdenoe.fr/" title="Au Comptoir de Noé"> adresse </a>. ACDN est une animelerie en ligne propuslée par <a class="link" href="https://www.prestashop.com/fr" title="Site de Prestashop">Prestashop</a> qui existe depuis plusieurs années, mais qui a été repris depuis un an.
                            <br>
                            Ce stage à été mené dans le cadre du stage de fin de deuxième année à MydigitalSchool. 
                            <br>
                            J'ai pu y découvrir l'environnement Prestashop qui possède une structure MVC. J'ai aussi été amené à utiliser <a href="https://sass-lang.com/" title="Site de SASS" target="_blank" class="link">SASS</a> (Sass est un préprocesseur CSS) que j'avais déjà touché briévement. Afin de compiler le SCSS j'ai aussi été amené à utiliser <a href="https://www.ruby-lang.org/fr/" class="link" target="_blank" title="Site de Ruby">Ruby</a> et en particulier la fonctionnalité "compass".
                            Pour la première fois j'ai utilisé un LazyLoad afin de charger les pages possédant de nombreuses images chargent plus vite.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>  

       <!-- section 5 - experiences  -->
     <section class="block-section section-title section-experiences">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3 class="title">
                            Anjou Web : Projet BOLeads
                        </h3>
                    </div>
                    <div class="col-12 col-sm-4 offset-sm-4">
                        <figure class="xp-img">
                            <img src="media/image/cv/anjouweb.png" alt="">
                        </figure>
                    </div>
                    <div class="col-12 col-sm-8 offset-sm-2">
                        <p class="paragraph">
                           Malgré le fait que l'entreprise se nomme AnjouWeb j'ai fait mon stage à Châteauroux. Durant deux mois j'ai travaillé sur un nouveau projet interne appelé BOLeads. BOLeads est un logiciel de marketing automation.
                        </p>
                        <p class="paragraph">
                            <strong>Définition :</strong>
                            <br>
                            Le Marketing Automation consiste à utiliser des logiciels et services Web pour exécuter, gérer et automatiser des tâches marketing, comme par exemple l'envoi d'e-mails personnalisés en fonction du comportement de l'utilisateur. (Source: <a href="https://www.1min30.com/dictionnaire-du-web/marketing-automation" class="link" target="_blank" title="Site de 1min30">www.1min30.com</a>)
                        </p>
                        <p class="paragraph">
                            L'objectif du stage était d'avancer le plus possible dans le développement de l'interface utilisateur du logiciel. Je devais créer une inteface sous forme de blocs triables et organisables, qui représentent chacun une tâche et qui soit lié à une ou plusieurs tâches.
                            <br>
                            Durant le stage j'ai appris à utiliser de nombreux outils que j'utilise énormément encore aujourd'hui, comme <a href="https://getbootstrap.com/" title="Site de Bootstrap" class="link" target="_blank">Bootstrap</a> ou <a href="https://jquery.com/JQuery" title="Site de JQuery" class="link" target="_blank">JQuery</a> pour les principaux.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>  

     <!-- Footer -->
    <?php

        include_once('includes/footer.php');    

    ?>


    <!-- Script -->
    <?php include('includes/script.php'); ?>
</body>
</html>