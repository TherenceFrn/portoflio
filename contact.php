<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- encodage de la page -->
    <meta charset="UTF-8">
    <!-- format de la page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- titre de la page  -->
    <title>Contact • Therence Ferron</title>
    <!-- description de la page -->
    <meta name="description" content="Retrouvez ici les liens vers mes réseaux sociaux, ainsi qu'un formulaire de contact.">
    <!-- keywords -->
    <meta name="keywords" content="developpeur web, angers, therence ferron, twitter, contact, formulaire de contact">
    <!-- author -->
    <meta name="author" content="Therence Ferron">
    <!-- copyright -->
    <meta name="copyright" content="Therence Ferron">


    <!-- Opengraph -->
    <?php include('includes/og.php'); ?>
    
    <!-- Twitter -->
    <?php include('includes/twitter.php'); ?>

    <!-- CSS -->
    <?php include('includes/css.php'); ?>

    <style> 
        /* FONT */

        @font-face {
            font-family: 'fontello';
            src: url('./font/fontello.eot?86727074');
            src: url('./font/fontello.eot?86727074#iefix') format('embedded-opentype'),
                url('./font/fontello.woff?86727074') format('woff'),
                url('./font/fontello.ttf?86727074') format('truetype'),
                url('./font/fontello.svg?86727074#fontello') format('svg');
            font-weight: normal;
            font-style: normal;
        }


        .demo-icon {
            font-family: "fontello" !important;
            font-style: normal;
            font-weight: normal;
            speak: none;
            display: inline-block;
            text-decoration: inherit;
            width: 1em;
            text-align: center;
            font-variant: normal;
            text-transform: none;
            line-height: 1em;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            /* text-shadow: 1px 1px 1px rgba(127, 127, 127, 0.3); */
        }
    </style>

    <?php

        
                

        if(isset($_POST['submitform'])) {

            if(!empty($_POST['nom']) AND !empty($_POST['subject']) AND !empty($_POST['email']) AND !empty($_POST['content'])){

                $nom = htmlspecialchars($_POST['nom']);
                $subject = htmlspecialchars($_POST['subject']);
                $email = htmlspecialchars($_POST['email']);
                $content = htmlspecialchars($_POST['content']);

                if (filter_var($email_b, FILTER_VALIDATE_EMAIL)) {

                    $header = "MIME-Version: 1.0\r\n";
                    $header .= 'From:"Therence Ferron"<contact@therenceferron.fr>'.'\n';
                    $header .= 'Content-Type:text/html; charset="utf-8"'.'\n';
                    $header .=  'Content-Transfer-Enconding: 8bit';
                    
                    $message = "
                        Nom de l'utilisateur : \r
                        ".$nom." \r \r
                        Email : \r
                        ".$email." \r \r
                        Sujet du mail : \r
                        ".$subject." \r \r
                        Message : \r
                        ".$content." \rf
                    ";

                    mail("contact@therenceferron.fr", $subject, $message, $header);

                    $msgForm = "Votre mail a bien été envoyé !";

                    unset($_POST['nom']);
                    unset($nom);
                    unset($_POST['subject']);
                    unset($subject);
                    unset($_POST['email']);
                    unset($email);
                    unset($_POST['content']);
                    unset($content);

                 } else {

                    $msgForm = "Votre adresse e-mail n'est pas valide !";

                }

            }

            else {

                $msgForm = "Tous les champs doivent être complétés !";

            }
        }


    ?>

</head>
<body>

    <!-- Section 1 -->
    <section class="block-section section-content">
        <div class="section-inner">
            <div class="projet-header">
                <!-- <figure class="projet-img">
                    <img src="media/image/projets-list/logo-japon-sans-texte.svg" alt="Therence Ferron Blog au Japon Developpeur Web Projet">
                </figure> -->
                <h1 class="title">
                    Contact
                </h1>
            </div>
        </div>
        <div id="scrollspy">
        </div>
    </section>

    <!-- HEADER + NAV + SURCOUCHE -->
    <?php include('includes/header-surcouche.php'); ?>

    <!-- section 2 - titre -->
    <section class="block-section section-title">
        <div class="section-inner">
            <h2 class="title">
                Mes réseaux sociaux
            </h2>
        </div>
    </section>
    
    <!-- section 3 - Slider -->
    <section class="block-section section-title contact-section">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <a href="https://twitter.com/therenceferron" target="_blank" title="Twitter @therenceferron">
                            <i class="demo-icon">&#xf099;</i>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="https://github.com/TherenceFrn" target="_blank" title="Github TherenceFrn">
                            <i class="demo-icon">&#xf09b;</i>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="https://www.linkedin.com/in/therence-ferron/" target="_blank" title="Linked-in Therence-Ferron">
                            <i class="demo-icon">&#xf0e1;</i>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="https://www.deviantart.com/celbilix" target="_blank" title="DeviantArt ">
                            <i class="demo-icon">&#xf1bd;</i>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="#" target="_blank" title="Youtube">
                            <i class="demo-icon">&#xf16a;</i>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="https://bitbucket.org/TherenceFrn/" target="_blank" title="BitBucket">
                            <i class="demo-icon">&#xf171;</i>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="https://sketchfab.com/" target="_blank" title="SketchFab">
                            <i class="demo-icon">&#xe807;</i>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="mailto:therence.ferron@gmail.com" target="_blank" title="Adresse e-mail">
                            <i class="demo-icon">&#xf0e0;</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- section 2 - titre -->
    <section class="block-section section-title">
        <div class="section-inner">
            <h2 class="title">
                Me contacter par mail
            </h2>
        </div>
    </section>

       <!-- section 2 - titre -->
    <section class="block-section section-title mail-contact">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-8">
                        <form action="" method="POST">
                            <div class="row">
                                <div class="col-12">
                                    <input type="text" name="nom" id="nom" class="contact-input" placeholder="Votre nom" value="<?php if(isset($_POST['nom'])){ echo $_POST['nom']; }?>">
                                </div>
                                <div class="col-12">
                                    <input type="text" name="subject" id="subject" class="contact-input" placeholder="Sujet du mail" value="<?php if(isset($_POST['subject'])){ echo $_POST['subject']; }?>">
                                </div>
                                <div class="col-12">
                                    <input type="email" name="email" id="email" class="contact-input" placeholder="Votre adresse e-mail" value="<?php if(isset($_POST['email'])){ echo $_POST['email']; }?>">
                                </div>
                                <div class="col-12">
                                    <textarea type="text" name="content" id="content" class="contact-input" placeholder="Contenu du mail"><?php if(isset($_POST['content'])){ echo $_POST['content']; }?></textarea>
                                </div>
                                 <?php

                                if(isset($msgForm)){?>
                                    <div class="col-12 erreur-form" style>
                                        <?php echo $msgForm; ?>
                                    </div>
                                <?php }

                                ?>
                                <div class="col-12">
                                    <button type="submit" name="submitform" id="submit-form" class="contact-submit">Envoyer</button>
                                </div>
                            </div>
                        </form>
                       
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="row">
                            <div class="col-12">
                                <p class="paragraph">
                                    Thérence Ferron
                                </p>
                            </div>
                            <div class="col-12">
                                <p class="paragraph">
                                    Angers, France
                                </p>
                            </div>
                            <div class="col-12">
                                <p class="paragraph">
                                    +33 6 47 79 47 08
                                </p>
                            </div>
                            <div class="col-12">
                                <p class="paragraph">
                                    therence.ferron[at]gmail.com
                                </p>
                                <p class="paragraph">
                                    contact[at]therenceferron.fr
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- section 2 - titre -->
    <section class="block-section section-title">
        <div class="section-inner">
            <h2 class="title">
                Mes contacts
            </h2>
        </div>
    </section>

    <!-- section 3 - Slider -->
    <section class="block-section section-title contacts-list">
        <div class="section-inner">
            <div class="container">
                <div class="row">

                    <div class="col-12 col-sm-4 col-md-3 contact-item">
                        <div class="row">
                            <div class="col-12 contact-icone">
                                <i class="demo-icon">&#xe807;</i> 
                            </div>
                            <div class="col-12 contact-site">
                                <a href="https://gwendal.io/" target="_blank" title="Site de Gwendal Paty">
                                    gwendal.io
                                </a>
                            </div>
                            <div class="col-12 contact-etudiant">
                                Gwendal Paty
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3 contact-item">
                        <div class="row">
                            <div class="col-12 contact-icone">
                                <i class="demo-icon">&#xe807;</i> 
                            </div>
                            <div class="col-12 contact-site">
                                <a href="https://william-camara.fr/" target="_blank" title="Site de Gwendal Paty">
                                    william-camara.fr
                                </a>
                            </div>
                            <div class="col-12 contact-etudiant">
                                William Camara
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3 contact-item">
                        <div class="row">
                            <div class="col-12 contact-icone">
                                <i class="demo-icon">&#xe807;</i> 
                            </div>
                            <div class="col-12 contact-site">
                                <a href="https://florentin-doreau.fr/" target="_blank" title="Site de Florentin Doreau">
                                    florentin-doreau.fr
                                </a>
                            </div>
                            <div class="col-12 contact-etudiant">
                                Florentin Doreau
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3 contact-item">
                        <div class="row">
                            <div class="col-12 contact-icone">
                                <i class="demo-icon">&#xe807;</i> 
                            </div>
                            <div class="col-12 contact-site">
                                <a href="https://quentinmasbernat.fr/" target="_blank" title="Site de Quentin Masbernat">
                                    quentinmasbernat.fr
                                </a>
                            </div>
                            <div class="col-12 contact-etudiant">
                                Quentin Masbernat
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4 col-md-3 contact-item">
                        <div class="row">
                            <div class="col-12 contact-icone">
                                <i class="demo-icon">&#xe807;</i> 
                            </div>
                            <div class="col-12 contact-site">
                                <a href="https://neooo.fr/" target="_blank" title="Site de Néo Roussel">
                                    neooo.fr
                                </a>
                            </div>
                            <div class="col-12 contact-etudiant">
                                 Néo Roussel
                            </div>
                        </div>
                    </div>

                    
                    <div class="col-12 col-sm-4 col-md-3 contact-item">
                        <div class="row">
                            <div class="col-12 contact-icone">
                                <i class="demo-icon">&#xe807;</i> 
                            </div>
                            <div class="col-12 contact-site">
                                <a href="https://melissabelleau.fr/" target="_blank" title="Site de Mélissa Belleau">
                                    melissabelleau.fr
                                </a>
                            </div>
                            <div class="col-12 contact-etudiant">
                                  Mélissa Belleau
                            </div>
                        </div>
                    </div>


                   
                </div>
            </div>
        </div>
    </section>

     <!-- Footer -->
    <?php

        include_once('includes/footer.php');

    ?>

      

<!-- Script -->
    <?php include('includes/script.php'); ?>
</body>
</html>