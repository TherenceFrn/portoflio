<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- encodage de la page -->
    <meta charset="UTF-8">
    <!-- format de la page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- titre de la page  -->
    <title>Projet Fazer • Therence Ferron</title>
    <!-- description de la page -->
    <meta name="description" content="Pour les projets de partiels de première année, en équipe de 4 nous avons créé un jeu: Fazer.">
    <!-- keywords -->
    <meta name="keywords" content="developpeur web, angers, therence ferron, feu, fazer, phaserJS, sprite, pixel art, photoshop">
    <!-- author -->
    <meta name="author" content="Therence Ferron">
    <!-- copyright -->
    <meta name="copyright" content="Therence Ferron">


    <!-- Opengraph -->
    <?php include('includes/og.php'); ?>
    
    <!-- Twitter -->
    <?php include('includes/twitter.php'); ?>

    <!-- CSS -->
    <?php include('includes/css.php'); ?>

</head>
<body>
    <!-- Retourner à la liste des projets : -->
    <div class="back-to-projets">
        <a href="projets">
            Retourner à la liste des projets
        </a>
    </div>

    <!-- Section 1 -->
    <section class="block-section section-content">
        <div class="section-inner">
            <div class="projet-header">
                <figure class="projet-img">
                    <img src="media/image/projets-list/fazer-logo.png" alt="Logo du jeu Fazer réalisé par Quentin Masbernat">
                </figure>
                <h1 class="title">
                    Jeu Fazer
                </h1>
            </div>
        </div>
        <div id="scrollspy">
        </div>
    </section>

    <!-- HEADER + NAV + SURCOUCHE -->
    <?php include('includes/header-surcouche.php'); ?>

    <!-- Section 2 - title -->
    <section class="block-section section-title section-projet-title">
        <div class="section-inner">
            <div class="projet-title">
                <h3 class="title">
                    Le projet
                </h3>
                <img class="projet-pepe" title="Pepe (La masquotte de mon portfolio)" src="media/image/pepe/pepe-thinking-nobg-color.png" alt="Therence Ferron Pepe Developpement Web Angers">
            </div>
        </div>
    </section>

    <!-- section 3 - title -->
    <section class="block-section section-title">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                           Pour valider la première année de notre bachelor à MyDigitalSchool, durant deux semaines à la fin de l'année nous sommes en partiels afin de développer un projet.
                           <br>
                           Alors, avec un groupe de 4 nous avons décider de créer un jeu-vidéo !
                        </p>
                    </div>
                    <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-fazer/logo.png" alt="Grand logo Fazer réalisé par Quentin Masbernat">
                    </div>
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                        Accompagné de <a href="https://quentinmasbernat.fr/" class="link" title="Site de Quentin Masbernat" target="_blank">Quentin Masbernat</a>, <a href="https://melissabelleau.fr/" class="link" title="Site de Melissa Belleau" target="_blank">Melissa Belleau</a> et Thomas Laserre nous avons créé un jeu en se basant sur le Framework <a href="https://phaser.io/" class="link" title="Site de PhaserJS" target="_blank">PhaserJS</a>.
                        Nous avions pour but de développer le jeu, que ce soit en terme de jouabilité ou tous les élements de décor et personnages. Ainsi qu'un site autour jeu avec un système d'inscription/connexion pour enregistrer le score des joueurs, une partie pour présenter l'équipe et enfin mettre en place une présence sur les réseaux-sociaux.
                        </p>
                    </div>
                      <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-fazer/Fazer-SKIN.gif" alt="Gif de skins de Fazer réalisés par toute l'équipe Fazer">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Section 2 - title -->
    <section class="block-section section-title section-projet-title">
        <div class="section-inner">
            <div class="projet-title">
                <h3 class="title">
                    Développement
                </h3>
                <img class="projet-pepe" title="Pepe (La masquotte de mon portfolio)" src="media/image/pepe/pepe-pc-bg-color.png" alt="Pepe réalisé par Thérence Ferron sur Photoshop">
            </div>
        </div>
    </section>

    <section class="block-section section-title">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                           Lorsque nous avons débuté le développement du jeu, nous nous sommes répartis les tâches afin d'aller le plus vite possible (seulement 2 semaines pour créer un jeu !). <a href="https://quentinmasbernat.fr/" class="link" title="Site de Quentin Masbernat" target="_blank">Quentin</a> a proposé d'uiliser le Framework <a href="https://phaser.io/" class="link" title="Site de PhaserJS" target="_blank">PhaserJS</a>, qu'il ne connaissait pas, mais qui allait lui permettre de développer plus facilement le jeu.
                        </p>
                    </div>
                    <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-fazer/BOUTIQUE.png" alt="Screenshot de la boutique du site Fazer">
                    </div>
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                           De mon côté, avec Thomas Laserre, je me suis occupé dans un premier temps de créer les sprites des personnages et ennemis. Un sprite c'est une suite d'images mises les unes à côté des autres, qui représente les animations que va avoir un personnage, exemple :
                       </p>
                    </div>
                    <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-fazer/sprite.png" alt="Exemple de sprite d'un ennemi du jeu Fazer">
                    </div>
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                           Nous avons dessiner un bon nombre de personnages, d'alternatives, créer les animations des bonus en jeu, et surtout créer tous les ennemis. Cela nous a pris la majeure partie des deux semaines. Il fallait créer plusieurs type d'animations pour chaque personnage, l'animation d'apparition, de marche, de mort, d'attaque ... Voici un aperçu de tous les personnages que nous avons fait :
                       </p>
                    </div>
                    <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-fazer/liste-skins.png" alt="Image ou l'on peut voir la liste des personnages du jeu Fazer">
                    </div>
                </div>
            </div>
        </div>
    </section>

        <!-- Section 2 - title -->
    <section class="block-section section-title section-projet-title">
        <div class="section-inner">
            <div class="projet-title">
                <h3 class="title">
                    Résultat
                </h3>
                <img class="projet-pepe" title="Pepe (La masquotte de mon portfolio)" src="media/image/pepe/pepe-simple-nobg-color.png" alt="Pepe réalisé par Thérence Ferron sur Photoshop">
            </div>
        </div>
    </section>

    <!-- section 3 - title -->
    <section class="block-section section-title">
        <div class="section-inner">
            <div class="container">
                <div class="row">
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                            Au bout des deux semaines de partiels, nous avons atteints nos objectifs: Créer un jeu complet, développer un site sur lequel on peut créer un compte et enregistrer ses scores, créer des réseaux sociaux. Le jeu était en ligne à cette adresse : <a href="http://fazer.yj.fr/" target="_blank" class="link" title="Ancien site Fazer">http://fazer.yj.fr/</a>, nous avons aussi créer un Twitter : <a href="https://twitter.com/TheFazersQuest" target="_blank" class="link" title="Ancien site Fazer">@TheFazersQuest</a>.
                            <br>
                            Avant le début de notre présentation pour les partiels nous avons partagé une vidéo teaser que j'ai réalisé. Puis nous avons mis en ligne le site et de nombreuses personnes se sont mise à jouer et à poster leurs scores sur Twitter.
                        </p>
                    </div>
                    <div class="col-8 offset-2 projet-img">
                        <img src="media/image/projet-fazer/fazer-loader.gif" alt="Animation de chargement du site Fazer réalisé par Quentin Masbernat">
                    </div>
                    <div class="col-8 offset-2">
                        <p class="paragraph">
                           Je vous mets en bonus la vidéo teaser que nous avons partagé sur les réseaux sociaux : 
                        </p>
                    </div>
                    <div class="col-8 offset-2 projet-img">
                        
                        <video class="" autoplay muted controls loop alt="Vidéo teaser du jeu Fazer réalisé par Thérence Ferron">
                            <source src="media/image/projet-fazer/teaser.mp4" type="video/mp4" alt="Vidéo teaser du jeu Fazer réalisé par Thérence Ferron">
                        </video>

                    </div>
                </div>
            </div>
        </div>
    </section>

     <!-- Footer -->
    <?php

        include_once('includes/footer.php');

    ?>


<!-- Script -->
    <?php include('includes/script.php'); ?>

    <!-- Schema Org - Article -->

    
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "NewsArticle",
            "dateModified" : "2020-05-18",
            "datePublished" : "2020-05-18",
            "headline": "Projet du jeu Fazer",
            "image": "https://therenceferron.fr/media/image/projet-list/logo-fazer.png",
            "author": {
                "@type" : "Corporation",
                "name" : "Thérence Ferron",
                "url" : "https://therenceferron.fr",
                "logo" : "https://therenceferron.fr/media/icon/icone.jpg"
            },
            "publisher": {
                "@type" : "Organization",
                "name" : "Thérence Ferron",
                "url" : "https://therenceferron.fr", 
                "logo": {
                    "@type": "ImageObject",
                    "name": "Logo Therence Ferron",
                    "width": "1000",
                    "height": "1000",
                    "url": "https://therenceferron.fr/media/icon/icone.jpg"
                }
            },
            "mainEntityOfPage" : "https://therenceferron.fr/projet-fazer"
            }
    </script>

</body>
</html>