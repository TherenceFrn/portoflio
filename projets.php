<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- encodage de la page -->
    <meta charset="UTF-8">
    <!-- format de la page -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- titre de la page  -->
    <title>Projets • Therence Ferron</title>
    <!-- description de la page -->
    <meta name="description" content="Retrouvez ici tous les projets sur lesquels j'ai travaillé, je détaille les étapes du développement du projet, et j'y expose les résultats.">
    <!-- keywords -->
    <meta name="keywords" content="developpeur web, angers, therence ferron, projets">
    <!-- author -->
    <meta name="author" content="Therence Ferron">
    <!-- copyright -->
    <meta name="copyright" content="Therence Ferron">


    <!-- Opengraph -->
    <?php include('includes/og.php'); ?>
    
    <!-- Twitter -->
    <?php include('includes/twitter.php'); ?>

    <!-- CSS -->
    <?php include('includes/css.php'); ?>

</head>
<body>

    <!-- Section 1 -->
    <section class="block-section section-content">
        <div class="section-inner">
            <div class="projet-header">
                <h1 class="title">
                    Projets
                </h1>
            </div>
        </div>
        <div id="scrollspy">
        </div>
    </section>

    <!-- HEADER + NAV + SURCOUCHE -->
    <?php include('includes/header-surcouche.php'); ?>

    

     <!-- Section 2 - title -->
    <section class="block-section section-title">
        <div class="section-inner">
            <h2 class="title">
                Mes Projets
            </h2>
        </div>
    </section>

    <!-- section 3 - content -->
    <section class="block-section section-title section-projets-list">
        <div class="section-inner">
            <div class="container">
                <div class="projets-list row">
                
                    <a href="projet-japon" class="projets-item col-12">
                        <h3 class="title">
                            Blog au Japon
                        </h3>
                        <figure class="projets-img">
                            <img src="media/image/projets-list/logo-japon-sans-texte.svg" alt="Logo du projet blog Japon de Therence Ferron">
                        </figure>
                    </a>

                       <a href="projet-fazer" class="projets-item col-12">
                        <h3 class="title">
                            Jeu Fazer
                        </h3>
                        <figure class="projets-img">
                            <img src="media/image/projets-list/fazer-logo.png" alt="Logo du jeu Fazer de Quentin Masbernat">
                        </figure>
                    </a>
                  
                  
                </div>
            </div>
        </div>
    </section>

     <!-- Footer -->
    <?php

        include_once('includes/footer.php');

    ?>


    <!-- Script -->
    <?php include('includes/script.php'); ?>
</body>
</html>